import React from 'react';

import { Redirect, Route, Switch } from 'react-router-dom';

import './ConteudoCentral.css';

import PaginaEspecialidades from './PaginaEspecialidades/PaginaEspecialidades';

import PaginaNovaReceita from './PaginaNovaReceita/PaginaNovaReceita';

import PaginaReceitas from './PaginaReceitas/PaginaReceitas';

import DetalhesReceita from './DetalhesReceita/DetalhesReceita.js';

import ReceitasPorEspecialidade from './ReceitasPorEspecialidade/ReceitasPorEspecialidade.js';

import PaginaNovaEspecialidade from './PaginaNovaEspecialidade/PaginaNovaEspecialidade';

    const ConteudoCentral = ( { listaEspecialidades, carregarEspecialidades } ) => {

    return (
        
        <main>

            <Switch>

                <Route exact path="/">
                    <Redirect to = "/Receitas"/>
                </Route>

                <Route path="/Receitas">
                    <PaginaReceitas />
                </Route>

                <Route path="/Nova-Receita">
                    <PaginaNovaReceita />
                </Route>

                <Route path="/Especialidades">
                    <PaginaEspecialidades listaEspecialidades = {listaEspecialidades}
                                          carregarEspecialidades = { carregarEspecialidades }/>
                </Route>

                <Route path="/Nova-Especialidade">
                    <PaginaNovaEspecialidade carregarEspecialidades = { carregarEspecialidades }/>
                </Route>

                <Route exact path="/Detalhes-Receita/:idReceita" >
                    <DetalhesReceita />
                </Route>

                <Route exact path="/Receitas-Por-Especialidade/:idEspecialidade" >
                    <ReceitasPorEspecialidade />
                </Route>

            </Switch>

        </main>
    );
};

export default ConteudoCentral;