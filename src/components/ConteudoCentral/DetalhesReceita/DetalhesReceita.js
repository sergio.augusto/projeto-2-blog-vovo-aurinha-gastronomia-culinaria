import React, { useContext, useEffect, useState } from 'react';

import { useHistory } from 'react-router';

import { useParams } from 'react-router-dom';

import './DetalhesReceita.css';

import TemaContext from "../../../contexts/TemaContext";

import MostraIngredientes from '../../../utils/MostraIngredientes.js';

import MostraModoPreparo from '../../../utils/MostraModoPreparo.js';

import Rating from '@material-ui/lab/Rating';

import Box from '@material-ui/core/Box';

import ImagemLike from '../../../../src/images/ImagemLike.png';

import ImagemDislike from '../../../../src/images/ImagemDislike.png';

import servicoAlterarReceita from '../../../utils/servicoAlterarReceita.js';

import servicoPegarDescricaoEspecialidade from '../../../utils/servicoPegarDescricaoEspecialidade';

import servicoListarUmaReceita from '../../../utils/servicoListarUmaReceita';

const DetalhesReceita = ( ) => {

    const tema = useContext (TemaContext);

    const history = useHistory();

    const [value, setValue] = useState(0);

    const [mostraAvaliacao, setMostraAvaliacao] = useState("none");
    const [mostraBtnAvaliarReceita, setMostraBtnAvaliarReceita] = useState("");
   
    const AvaliarReceita = ( ) => { 
        setMostraAvaliacao("")
        setMostraBtnAvaliarReceita("none")
    };

    const EnviarAvaliacao = ( ) => { 

        if ( value !== 0 && receitaSelecionada )
        {
            const receitaAvaliada = receitaSelecionada

            const qtdeXAvaliacoes = ( parseFloat( receitaAvaliada.receitaQtdeAvaliacoes ) ) * 
                                    ( parseFloat( receitaAvaliada.receitaValorAvaliacao ) )

            const qtde = parseFloat (receitaAvaliada.receitaQtdeAvaliacoes) + 1 
            
            receitaAvaliada.receitaQtdeAvaliacoes = qtde.toString()

            receitaAvaliada.receitaValorAvaliacao = ( parseFloat ( ( qtdeXAvaliacoes + value ) / qtde ) ).toString()

            setReceitaSelecionada(receitaAvaliada)

            setMostraAvaliacao("none")

            receitaAvaliada.receitaNumeroLike = numeroLike
            receitaAvaliada.receitaNumeroDislike = numeroDislike

            const receitaAlterada = {
                "receitaNumeroLike" : receitaAvaliada.receitaNumeroLike,
                "receitaNumeroDislike" : receitaAvaliada.receitaNumeroDislike,
                "receitaValorAvaliacao" : receitaAvaliada.receitaValorAvaliacao,
                "receitaQtdeAvaliacoes" : receitaAvaliada.receitaQtdeAvaliacoes
            }
        
            servicoAlterarReceita ( receitaAvaliada.receitaId, receitaAlterada )

            setTimeout(() => {

                history.push("/Receitas")

            }, 4000);

        }
        else
        {
            alert ( "Avaliação Inválida !" )
        }
       
    };

    const [jaCurtiu, setJaCurtiu] = useState (false);
    const [primeiraVezLike, setPrimeiraVezLike] = useState (true);
    const [numeroLike, setNumeroLike] = useState (0);

    const MostraNumeroLike = ( { Curtidas } ) => { 
        
        if ( primeiraVezLike )
        {
            setNumeroLike ( Curtidas )
            setPrimeiraVezLike ( false )
        }
        
        const CurtidasJSX = <div className="DR-div-numero-like">{ numeroLike }</div>
        
        return ( CurtidasJSX )
    };

    const somaUmLike = ( ) => { 

        if ( ! jaCurtiu )
        {
            setJaCurtiu ( true )
            setNumeroLike ( numeroLike + 1 )
        }
    };

    const [jaDiscurtiu, setJaDiscurtiu] = useState (false);
    const [primeiraVezDislike, setPrimeiraVezDislike] = useState (true);
    const [numeroDislike, setNumeroDislike] = useState (0);

    const MostraNumeroDislike = ( { naoCurtidas } ) => { 
        
        if ( primeiraVezDislike )
        {
            setNumeroDislike ( naoCurtidas )
            setPrimeiraVezDislike ( false )
        }
        
        const CurtidasJSX = <div className="DR-div-numero-dislike">{ numeroDislike }</div>
        
        return ( CurtidasJSX )
    };

    const somaUmDislike = ( ) => { 

        if ( ! jaDiscurtiu )
        {
            setJaDiscurtiu ( true )
            setNumeroDislike ( numeroDislike + 1 )
        }
    };

    const { idReceita } = useParams ();

    const [receitaSelecionada , setReceitaSelecionada] = useState ( {} );

    useEffect( () => {

        async function fetchData() {

            const _receitaSelecionada = await servicoListarUmaReceita ( idReceita );
            setReceitaSelecionada ( _receitaSelecionada )
        }

        fetchData();

    }, [ idReceita ]); 

    
    const [descricaoEspecialidade, setDescricaoEspecialidade] = useState ("");

    useEffect ( () => {

        async function fetchData() {

            const _descricaoEspecialidade = await servicoPegarDescricaoEspecialidade ( receitaSelecionada.receitaIdEspecialidade );
            setDescricaoEspecialidade( _descricaoEspecialidade );

        }

        fetchData();

    }, [ receitaSelecionada ])

    return (

        <div id="div-Detalhes-Receita" style={ { color:tema.corTexto, backgroundColor:tema.corFundoTema } } >

            { Object.keys ( receitaSelecionada ).length > 0 && 

                <div key={ receitaSelecionada.receitaId}>

                    <h3 className="DR-titulo">{ receitaSelecionada.receitaTitulo }</h3>

                    <p className="DR-postada-em"><b>Postada em : { receitaSelecionada.receitaPostadaEm }</b></p>

                    <p className="DR-especialidade">
                        <b>Especialidade : { descricaoEspecialidade }</b>
                    </p>

                    <div className="DR-div-imagem">
                        { receitaSelecionada.receitaImagem ?
                            receitaSelecionada.receitaImagem.includes('http') ?
                                <img className="DR-imagem-receita" src={ receitaSelecionada.receitaImagem } alt=""/> : null 
                        :null } 
                    </div>  

                    <div className="DR-texto-receita"> 

                        <p> <b> Ingredientes: </b> <br/><br/> </p>
            
                        <MostraIngredientes Ingredientes = { receitaSelecionada.receitaIngredientes }/>
                    
                        <p> <br/><b> Modo de preparo:</b> <br/><br/> </p>
                        
                        <MostraModoPreparo ModoPreparo = { receitaSelecionada.receitaModoPreparo }/>

                    </div>  

                    <div className="DR-div-like-dislike">

                        <div className="DR-div-like">

                            <div className="DR-div-imagem-like">
                                <img    
                                    src={ ImagemLike } 
                                    alt="" 
                                    className="DR-imagem-like" 
                                    onClick = { ( ) => { somaUmLike ( ) } } />
                            </div>

                            <MostraNumeroLike Curtidas={ parseInt ( receitaSelecionada.receitaNumeroLike ) } />

                        </div>
                    
                        <div className="DR-div-dislike">
        
                            <div className="DR-div-imagem-dislike">
                                <img 
                                    src={ ImagemDislike } 
                                    alt="" 
                                    className="DR-imagem-dislike" 
                                    onClick = { ( ) => { somaUmDislike ( ) } } />
                            </div>

                            <MostraNumeroDislike naoCurtidas={ parseInt ( receitaSelecionada.receitaNumeroDislike ) } />

                        </div>
                    
                    </div>

                    <div className="DR-avaliacoes">

                        <div className="DR-div-avaliacaoread">

                            <Box component="fieldset" mb={3} borderColor="transparent">
                                <Rating name="half-rating-read" precision={0.5}
                                        value={ parseFloat ( receitaSelecionada.receitaValorAvaliacao ) } 
                                        readOnly 
                                />
                            </Box>  

                            <div className="DR-div-qtde-avaliacoesread"> 
                                { receitaSelecionada.receitaQtdeAvaliacoes }
                            </div>

                        </div>

                        <div className="DR-div-avaliacao">
                
                            <button className="DR-div-btn-avaliar-receita"
                                    style = { { backgroundColor : tema.corFundoBtnContinueLendo,
                                                color : tema.corTextoBtnContinueLendo,
                                                display : mostraBtnAvaliarReceita } } 
                                    onClick = { ( ) => { AvaliarReceita () } }  >
                                Avaliar Receita 
                            </button>
                                                        
                            <Box    display={mostraAvaliacao} 
                                    component="fieldset" 
                                    mb={3} 
                                    borderColor="transparent">
                                <Rating
                                    name="half-rating" precision={0.5}
                                    value={ value }
                                    onChange={ ( event , newValue ) => { 
                                        setValue ( newValue ); } } 
                                />
                            </Box>  

                            <button className="DR-div-btn-envia-avaliacao"
                                    style = { { backgroundColor : tema.corFundoBtnContinueLendo,
                                                color : tema.corTextoBtnContinueLendo,
                                                display : mostraAvaliacao }} 
                                    onClick = { ( ) => { EnviarAvaliacao () } }  >
                                Enviar Avaliação... 
                            </button>

                        </div>

                    </div>

                </div>
            }  

        </div>
    );
};

export default DetalhesReceita;