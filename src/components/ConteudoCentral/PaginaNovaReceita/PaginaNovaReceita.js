import React, { useContext, useEffect, useState } from 'react';

import { useHistory } from 'react-router';

import TemaContext from '../../../contexts/TemaContext';

import './PaginaNovaReceita.css';

import servicoIncluirNovaReceita from '../../../utils/servicoIncluirNovaReceita.js';

import gerarDataPostagem from '../../../utils/gerarDataPostagem';

import servicoListarEspecialidades from '../../../utils/servicoListarEspecialidades';

const PaginaNovaReceita = ( ) => {

    const tema = useContext ( TemaContext );

    const [ receitaTitulo, setReceitaTitulo ] = useState ("");

    const [ receitaIdEspecialidade, setReceitaIdEspecialidade ] = useState ("");

    const [ receitaImagem, setReceitaImagem ] = useState ("");

    const [ receitaIngredientesTexto, setReceitaIngredientesTexto ] = useState ("");

    const [ receitaModoPreparoTexto, setReceitaModoPreparoTexto ] = useState ("");

    const [ mensagem, setMensagem ] = useState ("");

    const history = useHistory();

    const validarForm = () => {

        if ( ! receitaTitulo ) 
        {
            alert ("Favor, preencher o Título")
            return false
        }
        else
        {
            if ( receitaIdEspecialidade < "1" )
            {
                alert ("Favor, preencher a Especialidade")
                return false
            } 
            else
            {
                if ( ! receitaImagem )
                {
                    alert ("Favor, preencher Link da Imagem")
                    return false
                }
                else
                {
                    if ( ! receitaIngredientesTexto )
                    {
                        alert ("Favor, preencher Ingredientes")
                        return false
                    }
                    else
                    {
                        if ( ! receitaModoPreparoTexto )
                        {
                            alert ("Favor, preencher Modo de Preparo")
                            return false
                        }
                    }
                }
            }
        }
        return true;
    };

    const [listaEspecialidades, setListaEspecialidades] = useState ([]);
    
    useEffect ( () => {
        servicoListarEspecialidades ( setListaEspecialidades )
    }, [])

    const incluirNovaReceita = async ( evento ) => {

        evento.preventDefault();

        if ( !validarForm() ) 
        { 
            return false
        };

        const _receitaIngredientes = receitaIngredientesTexto.split("\n");
        const _receitaModoPreparo = receitaModoPreparoTexto.split("\n");

        const novaReceita = {
            "receitaTitulo" : receitaTitulo,
            "receitaPostadaEm" : gerarDataPostagem(),
            "receitaIdEspecialidade" : receitaIdEspecialidade,
            "receitaImagem" : receitaImagem,
            "receitaIngredientes" : _receitaIngredientes,
            "receitaModoPreparo" : _receitaModoPreparo ,
            "receitaNumeroLike" : "0",
            "receitaNumeroDislike" : "0",
            "receitaValorAvaliacao" : "0",
            "receitaQtdeAvaliacoes" : "0"
        }

        const resposta = await servicoIncluirNovaReceita ( novaReceita );

        setMensagem(resposta.mensagem);

        setTimeout(() => {

            if ( resposta.sucesso )
            {
                history.push("/Receitas")
            }
            else
            {
                setMensagem( "" );
                history.push("/Nova-Receita")
            }
        }, 3000);

    };

    return (

        <div    className="nr-novareceita" 
                style={ {   backgroundColor:tema.corFundoTema, 
                            color : tema.corTexto } } >

            <h3>Nova Receita</h3>

            { mensagem  ? <p id="nr-mensagem">Mensagem : { mensagem } </p>
                        : <p id="nr-mensagem">Mensagem : { null } </p>
            }

            <form onSubmit = { ( evento ) => { incluirNovaReceita ( evento ) } }>
            
                <div className="nr-campo">

                    <label htmlFor="nr-campo-titulo"><b>Título</b></label>
                    <input  
                        id="nr-campo-titulo" 
                        name="nr-campo-titulo"
                        placeholder="Entre com o Título da Receita"
                        value={ receitaTitulo }
                        onFocus = { () => { setMensagem(null) } }
                        onChange = { ( evento ) => { 
                            setReceitaTitulo ( evento.target.value ) } } 
                    />

                </div>

                <div className="nr-campo">

                    <label htmlFor="nr-campo-especialidade"><b>Especialidade</b></label>
                    <select 
                        id="nr-campo-especialidade" 
                        name="nr-campo-especialidade" 
                        placeholder="Escolha uma das Especialidades"
                        value={ receitaIdEspecialidade }
                        onFocus = { () => { setMensagem(null) } }
                        onChange = { ( evento ) => { 
                            setReceitaIdEspecialidade ( evento.target.value ) } } >

                        <option value={0} >Selecione uma Especialidade</option>

                        { listaEspecialidades.map( (item) => {
                            return ( 
                                <option value={ item.id } key={ item.id } >
                                    { item.descricao }
                                </option> )
                        } ) }
                    </select>

                </div>

                <div className="nr-campo">

                    <label htmlFor="nr-campo-imagem"><b>Imagem</b></label>
                    <input 
                        id="nr-campo-imagem" 
                        name="nr-campo-imagem"
                        placeholder="Entre com o Link da Imagem"
                        value={ receitaImagem } 
                        onFocus = { () => { setMensagem(null) } }
                        onChange = { ( evento ) => { 
                            setReceitaImagem ( evento.target.value ) } } />

                </div>

                <div className="nr-campo">

                    <label htmlFor="nr-ingredientes">
                        <b>Ingredientes</b>
                    </label>
                    <textarea 
                        id="nr-ingredientes" 
                        name="nr-ingredientes"
                        placeholder="Entre com os Ingredientes da Receita"
                        value={ receitaIngredientesTexto } 
                        onFocus = { () => { setMensagem(null) } }
                        onChange = { ( evento ) => { 
                            setReceitaIngredientesTexto ( evento.target.value )
                        } } />

                </div>

                <div className="nr-campo">

                    <label htmlFor="nr-modo-preparo">
                        <b>Modo de Preparo</b>
                    </label>
                    <textarea 
                        id="nr-modo-preparo" 
                        name="nr-modo-preparo" 
                        placeholder="Entre com o Modo de Preparo da Receita"
                        value={ receitaModoPreparoTexto }
                        onFocus = { () => { setMensagem(null) } }
                        onChange = { ( evento ) => { 
                            setReceitaModoPreparoTexto ( evento.target.value ) 
                        } } />

                </div>

                <button style = { { backgroundColor : tema.corFundoBtnContinueLendo,
                                    color : tema.corTextoBtnContinueLendo } } >
                    Incluir
                </button>

            </form>

        </div>

    );
    
};

export default PaginaNovaReceita;