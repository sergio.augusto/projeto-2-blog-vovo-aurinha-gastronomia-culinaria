import React, { useContext } from 'react';

import { Link } from 'react-router-dom';

import './PaginaEspecialidades.css';

import TemaContext from '../../../contexts/TemaContext.js';

import servicoExcluirEspecialidade from '../../../utils/servicoExcluirEspecialidade';

const PaginaEspecialidades = ( { listaEspecialidades, carregarEspecialidades } ) => {

    const tema = useContext (TemaContext);
 
    const excluirEspecialidade = async ( idEspecialidade ) => {

        const exclui = window.confirm ('Confirma Exclusão Especialidade ?');

        if ( exclui )
        {
            const resposta = await servicoExcluirEspecialidade ( idEspecialidade );

            if ( resposta.sucesso )
            {
                alert ( resposta.mensagem )
                carregarEspecialidades()
                return false
            }
            alert ( resposta.mensagem );
        }
    };

    return (

        <div id="div-Especialidades-Nova-Especialidade"
             style={{ backgroundColor:tema.corFundoTema, 
                      color : tema.corTexto } } >

            <div id="div-Especialidades">

                <h3 id="h2-Titulo-Especialidades">Especialidades : </h3>

                <ul id="ul-Especialidades">

                    { listaEspecialidades.map ( ( item ) => { 
    
                        return (

                            <li className="li-Especialidades" key={item.id}>

                                <button className="btn-Exclui-Especialidade"
                                        style = { { backgroundColor : tema.corFundoBtnContinueLendo,
                                                    color : tema.corTextoBtnContinueLendo } } 
                                        onClick = { ( ) => { excluirEspecialidade ( item.id ) } } >
                                    Excluir    
                                </button>

                                <Link   to = {`/Receitas-Por-Especialidade/${item.id}`}
                                        className="Receitas-Por-Especialidade" 
                                        style={ { color : tema.corTexto } } >

                                    <b>{item.descricao}</b> 
                                    
                                </Link>

                            </li> 

                        );

                    } ) }

                </ul>

            </div>

            {/* <div id="div-Nova-Especialidade">

                <Link   to='/NovaEspecialidade'
                        id="btn-Nova-Especialidade"
                        style = { { color : tema.corTexto } } >
                    <b>Nova Especialidade</b>
                </Link>

            </div> */}

        </div>

    );
};

export default PaginaEspecialidades;
