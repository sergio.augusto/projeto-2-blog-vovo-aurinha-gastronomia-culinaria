import React, { useContext, useState } from 'react';

import { useHistory } from 'react-router';

import TemaContext from '../../../contexts/TemaContext';

import './PaginaNovaEspecialidade.css';

import servicoIncluirNovaEspecialidade from '../../../utils/servicoIncluirNovaEspecialidade.js';

const PaginaNovaEspecialidade = ( { carregarEspecialidades } ) => {

    const tema = useContext ( TemaContext );

    const [ especialidadeDescricao, setEspecialidadeDescricao ] = useState ("");

    const [ mensagem, setMensagem ] = useState ("");

    const history = useHistory();

    const incluirNovaEspecialidade = async ( evento ) => {

        evento.preventDefault();

        if ( ! especialidadeDescricao ) 
        {
            alert ("Favor, preencher a Especialidade")
            return false
        }

        const novaEspecialidade = { "descricao" : especialidadeDescricao }

        const resposta = await servicoIncluirNovaEspecialidade ( novaEspecialidade );

        setMensagem( resposta.mensagem );

        setTimeout(() => {

            if ( resposta.sucesso )
            {
                carregarEspecialidades ()
                history.push("/Especialidades")
            }
            else
            {
                setMensagem( "" );
                history.push("/Nova-Especialidade")
            }
        }, 3000);

    };

    return (

        <div className="ne-novaespecialidade"   style={ {   backgroundColor:tema.corFundoTema, 
                                                color : tema.corTexto } } >

            <h3>Nova Especialidade</h3>

            { mensagem  ? <p id="ne-mensagem">Mensagem : { mensagem } </p>
                        : <p id="ne-mensagem">Mensagem : { null } </p>
            }

            <form onSubmit = { ( evento ) => { incluirNovaEspecialidade ( evento ) } }>
            
                <div className="ne-campo">

                    <label htmlFor="ne-campo-descricao"><b>Descrição da Especialidade</b></label>
                    <input  
                        id="ne-campo-descricao" 
                        name="ne-campo-descricao"
                        placeholder="Entre com a nova Especialidade"
                        value={ especialidadeDescricao }
                        onFocus = { () => { setMensagem(null) } }
                        onChange = { ( evento ) => { 
                            setEspecialidadeDescricao ( evento.target.value ) } } 
                    />

                </div>

                <button style = { { backgroundColor : tema.corFundoBtnContinueLendo,
                                    color : tema.corTextoBtnContinueLendo } } >
                    Incluir
                </button>

            </form>

        </div>

    );
    
};

export default PaginaNovaEspecialidade;