import React, { useState, useEffect, useContext } from 'react';

import { useParams, useHistory } from 'react-router-dom';

import './ReceitasPorEspecialidade.css';

import TemaContext from '../../../contexts/TemaContext';

import servicoListarReceitasPorEspecialidade from '../../../utils/servicoListarReceitasPorEspecialidade';

import servicoPegarDescricaoEspecialidade from '../../../utils/servicoPegarDescricaoEspecialidade';

import Receita from '../../../../src/components/ConteudoCentral/PaginaReceitas/Receita/Receita.js';

const ReceitasPorEspecialidade = ( ) => {

    const { idEspecialidade } = useParams ();

    const tema = useContext (TemaContext);

    const history = useHistory();

    const [ receitasPorEspecialidade , setReceitasPorEspecialidade ] = useState ( [] );

     useEffect ( () => {

        async function fetchData() {

            const _receitasPorEspecialidade = await servicoListarReceitasPorEspecialidade ( idEspecialidade )

            setReceitasPorEspecialidade( _receitasPorEspecialidade.data )

            if ( ! _receitasPorEspecialidade.sucesso )
            {
                setTimeout(() => { history.push("/Especialidades") }, 3000);
            }
        }

        fetchData();
    
    }, [ idEspecialidade, history ])

    const [ descricaoEspecialidade , setDescricaoEspecialidade ] = useState ( "" );

    useEffect ( () => {

        async function fetchData() {

            const _descricaoEspecialidade = await servicoPegarDescricaoEspecialidade ( idEspecialidade )

            setDescricaoEspecialidade( _descricaoEspecialidade )
        }

        fetchData();
    
    }, [ idEspecialidade ])

    return (

        <div>

            { receitasPorEspecialidade.length > 0 ?

                receitasPorEspecialidade.map ( ( item ) => {

                    return(

                        <Receita 
                            key={item.receitaId}
                            receitaId={item.receitaId}
                            receitaTitulo={item.receitaTitulo}
                            receitaPostadaEm={item.receitaPostadaEm}
                            receitaIdEspecialidade={item.receitaIdEspecialidade}
                            receitaImagem={item.receitaImagem}
                            receitaIngredientes={item.receitaIngredientes}
                            receitaModoPreparo={item.receitaModoPreparo}
                            receitaNumeroLike={item.receitaNumeroLike}
                            receitaNumeroDislike={item.receitaNumeroDislike}
                            receitaValorAvaliacao={item.receitaValorAvaliacao}
                            receitaQtdeAvaliacoes={item.receitaQtdeAvaliacoes}
                            top="Especialidades"
                        />
                    )
                })

                :

                <p id="p-sem-receitas" style={ { backgroundColor:tema.corFundoTema, color : tema.corTexto } } >
                    <b>Não existe receita para a Especialidade : { descricaoEspecialidade }</b>
                </p>
           
            }

        </div>

    );
};

export default ReceitasPorEspecialidade;