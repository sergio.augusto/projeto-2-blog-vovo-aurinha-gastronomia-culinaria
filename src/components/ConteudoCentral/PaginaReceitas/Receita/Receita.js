import React, { useContext, useState, useEffect } from 'react';

import { Link } from 'react-router-dom'

import { useHistory } from 'react-router';

import TemaContext from '../../../../contexts/TemaContext.js';

import servicoExcluirReceita from '../../../../utils/servicoExcluirReceita.js';

import './Receita.css';

import Rating from '@material-ui/lab/Rating';

import Box from '@material-ui/core/Box';

import ImagemLike from '../../../../images/ImagemLike.png';

import ImagemDislike from '../../../../images/ImagemDislike.png';

import servicoPegarDescricaoEspecialidade from '../../../../utils/servicoPegarDescricaoEspecialidade.js';

import MostraIngredientes from '../../../../utils/MostraIngredientes.js';

import MostraModoPreparo from '../../../../utils/MostraModoPreparo.js';

const Receita = ( props ) => {

    const tema = useContext (TemaContext);

    const history = useHistory();

    const [ receitaExcluida, setReceitaExcluida ] = useState ( false );

    useEffect ( () => { 

        PegarDescricaoEspecialidade( props.receitaIdEspecialidade );

    } , [ props.receitaIdEspecialidade ] );

    const [descricaoEspecialidade, setDescricaoEspecialidade] = useState ("");
    
    const PegarDescricaoEspecialidade = async ( IdEspec ) => { 

        const descricao = await servicoPegarDescricaoEspecialidade ( IdEspec );

        setDescricaoEspecialidade(descricao);

    };

    const excluirReceita = async () => {

        const exclui = window.confirm ('Confirma Exclusão Receita ?');

        if ( exclui )
        {
            const resposta = await servicoExcluirReceita ( props.receitaId );

            if ( resposta.sucesso )
            {
                alert ( resposta.mensagem )
                setReceitaExcluida ( true )
                return false
            }
            alert ( resposta.mensagem );
        }
    };
 
    return (

        <>

            { receitaExcluida ? null :
            
                <article className="receita" style={{   backgroundColor:tema.corFundoTema, 
                                                        color : tema.corTexto }}>

                    <div className="div-todos-elementos">

                        <div className="div-receita-titulo-btn-excluir">

                            <h3 className="receita-titulo">{ props.receitaTitulo }</h3>

                            <div className="div-btn-excluir-back">

                                <button className="btn-excluir-receita" 
                                        style = { { backgroundColor : tema.corFundoBtnContinueLendo,
                                                    color : tema.corTextoBtnContinueLendo } } 
                                        onClick = { () => { excluirReceita ( ) } } >
                                    Excluir
                                </button>

                                <button className="btn-back-to-top" 
                                        style = { { backgroundColor : tema.corFundoBtnContinueLendo,
                                                    color : tema.corTextoBtnContinueLendo } } 
                                        onClick = { () => { setTimeout( () => { 
                                            if ( props.top === "Receitas" )
                                            {
                                                history.push( "/" )
                                            }
                                            else
                                            {
                                                history.push( "/Especialidades" ) 
                                            }
                                            }, 100); } } >
                                    Voltar
                                </button>

                            </div>

                        </div>

                        <p className="receita-postada-em"><b>Postada em : { props.receitaPostadaEm }</b></p>

                        <p className="DR-especialidade">
                            <b>Especialidade : { descricaoEspecialidade }</b>
                        </p>

                        <div className="div-receita-imagem-texto">

                            <div>
                                { props.receitaImagem ?
                                    props.receitaImagem.includes('http') ? <img className="imagem-receita" src={ props.receitaImagem } alt=""/> : null
                                :null }
                            </div>

                            <div className="texto-receita"> 

                                <p> <b> Ingredientes: </b> <br/><br/> </p>
                    
                                <MostraIngredientes Ingredientes = { props.receitaIngredientes }/>
                            
                                <p> <br/><b> Modo de preparo:</b> <br/><br/> </p>
                                
                                <MostraModoPreparo ModoPreparo = { props.receitaModoPreparo }/>

                            </div>

                        </div>

                        <div className="div-like-dislike-avaliacao-btn">

                            <div className="div-like-dislike">

                                <div className="div-imagem-like">
                                    <img    
                                        src={ ImagemLike } 
                                        alt="" 
                                        className="imagem-like" 
                                    />
                                </div>

                                <div className="div-numero-like">
                                    { props.receitaNumeroLike }
                                </div>

                                <div className="div-imagem-dislike">
                                    <img 
                                        src={ ImagemDislike } 
                                        alt="" 
                                        className="imagem-dislike" 
                                    />
                                </div>

                                <div className="div-numero-dislike">    
                                    { props.receitaNumeroDislike }
                                </div>

                            </div>

                            <div className="div-avaliacao">

                                <div className="div-estrelas">

                                    <Box component="fieldset" mb={3} borderColor="transparent">
                                        <Rating 
                                            name="half-rating-read" 
                                            value={ parseFloat ( props.receitaValorAvaliacao ) } 
                                            precision={0.5}
                                            readOnly 
                                        />
                                    </Box>

                                </div>

                                <div className="div-qtde-avaliacoes">    
                                    { props.receitaQtdeAvaliacoes }
                                </div>

                            </div>

                            <div>

                                <Link to = { `/Detalhes-Receita/${ props.receitaId }`}
                                    className="btn-receita-completa"
                                    style = { { backgroundColor : tema.corFundoBtnContinueLendo,
                                                color : tema.corTextoBtnContinueLendo }} >
                                    Ver Receita Completa ... 
                                </Link>

                            </div>

                        </div>

                    </div>

                </article>

            }

        </>

    )
};

export default Receita;