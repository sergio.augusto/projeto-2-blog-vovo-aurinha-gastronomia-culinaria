import React, { useState, useEffect } from 'react';

import './PaginaReceitas.css';

import Receita from './Receita/Receita';

import servicoListarReceitas from '../../../utils/servicoListarReceitas';

const PaginaReceitas = ( ) => {

    const [ todasReceitas, setTodasReceitas] = useState ([]);

    useEffect ( () => {

        servicoListarReceitas(setTodasReceitas);

    }, [] )

    return (

        <div>

            { todasReceitas.map ( ( item ) => {

                return(

                    <Receita 
                        key={item.receitaId}
                        receitaId={item.receitaId}
                        receitaTitulo={item.receitaTitulo}
                        receitaPostadaEm={item.receitaPostadaEm}
                        receitaIdEspecialidade={item.receitaIdEspecialidade}
                        receitaImagem={item.receitaImagem}
                        receitaIngredientes={item.receitaIngredientes}
                        receitaModoPreparo={item.receitaModoPreparo}
                        receitaNumeroLike={item.receitaNumeroLike}
                        receitaNumeroDislike={item.receitaNumeroDislike}
                        receitaValorAvaliacao={item.receitaValorAvaliacao}
                        receitaQtdeAvaliacoes={item.receitaQtdeAvaliacoes}
                        top="Receitas"
                    />
                )
            })}

        </div>
      
    );
};

export default PaginaReceitas;