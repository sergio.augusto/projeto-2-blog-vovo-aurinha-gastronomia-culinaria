import React, { useContext } from 'react';

import './ListaEspecialidades.css';

import TemaContext from '../../../contexts/TemaContext.js';

import { Link } from 'react-router-dom';

const ListaEspecialidades = ( { listaEspecialidades } ) => {

    const tema = useContext (TemaContext);

    return (

        <div id="div-BL-Especialidades" style={ {   backgroundColor : tema.corFundoTema,
                                                    color : tema.corTexto } }  >

            <h3 id="h3-BL-Especialidades">Especialidades :</h3>

            <ul id="ul-BL-Especialidades">

                { listaEspecialidades.map( ( item ) => { 
    
                    return ( 
                                <li key={item.id} className="li-BL-Especialidades">
                                    <Link   to = {`/Receitas-Por-Especialidade/${item.id}`}
                                            style={ { color : tema.corTexto } }  
                                            className="a-BL-Especialidade" key={item.id}>
                                        <b>{item.descricao}</b>
                                    </Link>
                                </li> 
                            );
                } ) }

            </ul>

        </div>

    );

};

export default ListaEspecialidades;