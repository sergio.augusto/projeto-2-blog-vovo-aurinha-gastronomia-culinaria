import React from 'react';

import './BarraLateral.css';

import ListaEspecialidades from "./ListaEspecialidades/ListaEspecialidades.js";

const BarraLateral = ( { listaEspecialidades } ) => {

    return (

        <div>

            <ListaEspecialidades listaEspecialidades = { listaEspecialidades } />

        </div>

    );
};

export default BarraLateral;