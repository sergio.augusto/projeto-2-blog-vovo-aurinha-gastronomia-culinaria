import React, { useContext } from 'react';

import { Link } from "react-router-dom";

import './Cabecalho.css';

import MenuPrincipal from './MenuPrincipal/MenuPrincipal';

import ImagemVovó from '../../images/VovóTransparente2.jpg';

import TemaContext from '../../contexts/TemaContext.js';

import { COR_1, COR_2, COR_3, COR_PADRAO } from '../../utils/coresTema.js';

const Cabecalho = ( { funcaoConfiguraTema } ) => {

    const tema = useContext (TemaContext);

    return (

        <header>

            <div className="div-h1-imagem-temas" style={ { color:tema.corTexto } } >

                <div id="div-h1-imagem">

                    <div>
                        <h1>
                            <Link to ="/" id="h1-link" style={ { color:tema.corTexto } } >
                                Blog Vovó Aurinha - Gastronomia e Culinária
                            </Link>
                        </h1>
                    </div>

                    <div>
                        <img className="imagem-vovo" src={ImagemVovó} alt=""/>   
                    </div>

                </div>

                <div id="div-temas">

                    <p><b>Tema de Cores</b></p>

                    <button id="btn-rosa" onClick={ () => { funcaoConfiguraTema ( COR_1 ) } } >
                        Rosa
                    </button>

                    <button id="btn-verde" onClick={ () => { funcaoConfiguraTema ( COR_2 ) } } >
                        Verde
                    </button>

                    <button id="btn-azul" onClick={ () => { funcaoConfiguraTema ( COR_3 ) } } >
                        Azul
                    </button>

                    <button id="btn-branco" onClick={ () => { funcaoConfiguraTema ( COR_PADRAO ) } } >
                        Branco
                    </button>

                </div>

            </div>

            <MenuPrincipal/>

        </header>
    );
};

export default Cabecalho;