import React, { useContext } from 'react';

import './Busca.css';

import TemaContext from "../../../../contexts/TemaContext.js";

const Busca = () => {

    const tema = useContext (TemaContext);

    return (

        <div id="div-busca" style={ { color:tema.corTexto, backgroundColor:tema.corFundoTema } } >

            <input 
                id="campo-busca"
                placeholder="Busque aqui ..."
                title="Busque aqui ..."
            ></input>

            <button id="btn-busca">
                <img    id = "img-lupa"
                        src="../../../../images/LupaTransparente.png" 
                        alt="Imagem de Lupa representando uma busca."/>
            </button>

        </div>

    );
};

export default Busca;