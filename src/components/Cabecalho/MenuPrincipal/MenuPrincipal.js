import React, {useContext} from 'react';

import { Link } from 'react-router-dom';

import TemaContext from '../../../contexts/TemaContext.js';

import './MenuPrincipal.css';

import Busca from '../MenuPrincipal/Busca/Busca.js';

const MenuPrincipal = () => {

    const tema = useContext (TemaContext);

    return (

        <nav style={ { backgroundColor:tema.corFundoTema } } >

            <ul>

                <li> 
                    {/* <a href="/Receitas">Receitas</a> */}
                    <Link to='/Receitas' style={ { color:tema.corTexto } } ><b>Receitas</b></Link>
                </li>

                <li> 
                    {/* <a href="/Nova Receita">Nova Receita</a> */}
                    <Link to='/Nova-Receita' style={ { color:tema.corTexto } } ><b>Nova Receita</b></Link>
                </li>

                <li> 
                    {/* <a href="/Especialidades">Especialidades</a> */}
                    <Link to='/Especialidades' style={ { color:tema.corTexto } } ><b>Especialidades</b></Link>
                </li>

                <li> 
                    {/* <a href="/Nova Especialidade">Nova Especialidade</a> */}
                    <Link to='/Nova-Especialidade' style={ { color:tema.corTexto } } ><b>Nova Especialidade</b></Link>
                </li>

            </ul>

            <Busca/>

        </nav>
    );
};

export default MenuPrincipal;