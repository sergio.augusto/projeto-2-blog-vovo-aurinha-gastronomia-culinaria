import React from 'react';

import './BarraPropaganda.css';

import Propaganda from './Propaganda/Propaganda';

const BarraPropaganda = () => {

    return (

        <aside id="aside-barra-propaganda">

            <Propaganda/>

        </aside>
    );
};

export default BarraPropaganda;