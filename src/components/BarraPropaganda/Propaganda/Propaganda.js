import React from 'react';

import Carousel, { autoplayPlugin } from '@brainhubeu/react-carousel';

import '@brainhubeu/react-carousel/lib/style.css';

import './Propaganda.css';

import PropagandaAcucarUniao from '../../../images/PropagandaAcucarUniao.jpg' ;

import PropagandaLeiteNinho from '../../../images/PropagandaLeiteNinho.jpg';

import PropagandaChocolateNestle from '../../../images/PropagandaChocolateNestle.jpg';

import PropagandaFarinhaTrigoBoaSorte from '../../../images/PropagandaFarinhaTrigoBoaSorte.jpg';

import PropagandaAchocolatadoNescau from '../../../images/PropagandaAchocolatadoNescau.png';

const Propaganda = ( ) => {

    return (

        // <Carousel   plugins = { [ 'arrows', 'infinite', { resolve: autoplayPlugin, options: { interval: 2000, } }, ]}   
        //             animationSpeed={1000} >  

        <Carousel   plugins = { [ 'infinite', { resolve: autoplayPlugin, options: { interval: 2000, } }, ]}   
                    animationSpeed={1000} > 

            <img  src={ PropagandaAcucarUniao } alt="" className="propaganda"/>

            <img  src={ PropagandaLeiteNinho } alt="" className="propaganda"/>

            <img  src={PropagandaChocolateNestle} alt="" className="propaganda"/>

            <img  src={ PropagandaFarinhaTrigoBoaSorte } alt="" className="propaganda"/>

            <img  src={ PropagandaAchocolatadoNescau } alt="" className="propaganda"/>

        </Carousel>
    );
};

export default Propaganda;
