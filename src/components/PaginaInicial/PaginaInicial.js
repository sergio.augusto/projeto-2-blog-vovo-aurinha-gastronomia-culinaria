import React, { useState, useEffect } from 'react';

import { BrowserRouter } from 'react-router-dom';

import TemaContext from '../../contexts/TemaContext.js'

import './PaginaInicial.css';

import Cabecalho from '../Cabecalho/Cabecalho';

import ConteudoCentral from '../ConteudoCentral/ConteudoCentral'

import BarraLateral from '../BarraLateral/BarraLateral';

import BarraPropaganda from '../BarraPropaganda/BarraPropaganda';

import Rodape from '../Rodape/Rodape';

import { COR_1_CONFIG, COR_2_CONFIG, COR_3_CONFIG, COR_PADRAO_CONFIG } from '../../utils/coresTema.js';

import { COR_1, COR_2, COR_3 } from '../../utils/coresTema.js';

import servicoListarEspecialidades from '../../utils/servicoListarEspecialidades.js';

const PaginaInicial = () => {

    const [ listaEspecialidades , setListaEspecialidades ] = useState ( [] );

    useEffect ( () => { 
        carregarEspecialidades ()
    } , [] );

    const carregarEspecialidades = () => { servicoListarEspecialidades( setListaEspecialidades ); }

    const [tema, setTema] = useState ( COR_PADRAO_CONFIG );

    const modificarTema = ( temaSelecionado ) => {

        switch ( temaSelecionado )
        {
            case COR_1 :
                setTema ( COR_1_CONFIG );
            break;

            case COR_2 :
                setTema ( COR_2_CONFIG );
            break;

            case COR_3 :
                setTema ( COR_3_CONFIG );
            break;

            default :
                setTema ( COR_PADRAO_CONFIG );
            break;
        }
    };

    return (

        <TemaContext.Provider value = { tema }>

            <BrowserRouter>

                <div id='box-pagina-inicial'>

                    <Cabecalho funcaoConfiguraTema = { modificarTema }/>

                    <div id='container'>

                        <BarraPropaganda/>

                        <ConteudoCentral listaEspecialidades = {listaEspecialidades}
                                        carregarEspecialidades = { carregarEspecialidades }/>

                        <BarraLateral listaEspecialidades = {listaEspecialidades} />

                    </div>

                    <Rodape/>

                </div>
                
            </BrowserRouter>

        </TemaContext.Provider>
    );
};

export default PaginaInicial;