import React, {useContext} from 'react';

import TemaContext from '../../contexts/TemaContext.js';

import './Rodape.css';

const Rodape = () => {

    const tema = useContext (TemaContext);

    return (

        <footer style={ { backgroundColor:tema.corFundoTema, color:tema.corTexto } } >

            <b>Copyright &#169; Infnet</b>

        </footer>

    );
};

export default Rodape;