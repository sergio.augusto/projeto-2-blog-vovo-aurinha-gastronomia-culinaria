import axios from '../axios/axios';

const servicoExcluirReceita = async ( IdReceita ) => {

    try 
    {
        const resposta = await axios.delete(`receita/${ IdReceita }`)

        console.log( `Resposta : ${ JSON.stringify( resposta.data ) } ` )
        
        return ( { 'sucesso': true, 'mensagem': "Receita Excluída." } )
    } 
    catch ( error ) 
    {
        if ( error.response && error.response.status )
        {
            switch ( error.response.status ) 
            {
                case 409:
                    return( { 'sucesso': false, 'mensagem': error.response.data.message } );


                default:
                    return ( { 'sucesso': false, 'mensagem': "Ocorreu algum problema na exclusão da Receita." } );

            }
        }
        else
        {
            return ( { 'sucesso': false, 'mensagem': "O servidor está fora do ar... Tente Mais Tarde..." } );
        }
    }
};

export default servicoExcluirReceita;