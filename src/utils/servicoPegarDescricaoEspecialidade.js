import axios from '../axios/axios';

const servicoPegarDescricaoEspecialidade = async ( idEspecialidade ) => {

    try {

        const resposta = await axios.get(`especialidade/${ idEspecialidade }`)

        console.log( `Resposta : ${ JSON.stringify( resposta.data ) } ` )
    
        return ( resposta.data.result.descricao )

    } catch (erro) {

        if ( erro.response && erro.response.status )
        {
            switch ( erro.response.status ) 
            {
                case 409:
                    console.log ( erro.response.data.message )
                break
                
                default:
                    console.log ("Problemas no cadastramento da Nova Receita" )
                break
                    
            }
        }
        else
        {
            alert ( "O servidor está fora do ar... Tente Mais Tarde..." )
        }
    }
};

export default servicoPegarDescricaoEspecialidade;