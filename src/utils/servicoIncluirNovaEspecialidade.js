import axios from '../axios/axios';

const servicoIncluirNovaEspecialidade = async ( novaEspecialidade ) => {

    try 
    {
        const resposta = await axios.post("especialidade", novaEspecialidade )

        console.log( `Resposta : ${ JSON.stringify( resposta.data ) } ` )

        return ( { 'sucesso' : true, 'mensagem' : "Nova Especialidade cadastrada com sucesso..." } )
    }
    catch ( error ) 
    {
        if ( error.response && error.response.status )
        {
            switch ( error.response.status ) 
            {
                case 409:
                    return ( { 'sucesso' : false, 'mensagem' : error.response.data.message } )
                
                default:
                    return ( { 'sucesso' : false, 'mensagem' : "Problemas no cadastramento da Nova Especialidade" } )            
            }
        }
        else
        {
            return ( { 'sucesso' : false, 'mensagem' : "O servidor está fora do ar... Tente Mais Tarde..." } )
        }
    }
};

export default servicoIncluirNovaEspecialidade;