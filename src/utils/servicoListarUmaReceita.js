import axios from '../axios/axios';

const servicoListarUmaReceita = async ( idReceita ) => {

    try 
    {
        const resposta = await axios.get(`receita/${ idReceita }`)

        console.log( `Resposta servico listar UMA Receita : ${ JSON.stringify( resposta.data ) } ` )

        return ( resposta.data.result )
    }
    catch (erro) 
    {
        if ( erro.message )
        {
            console.log(erro.message)
        }
        else
        {
            alert ("O servidor está fora do ar... Tente Mais Tarde...")
        }      
    }
};

export default servicoListarUmaReceita;