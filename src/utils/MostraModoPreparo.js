import React from "react";

const MostraModoPreparo = ( { ModoPreparo } ) => {

    let ind = 1;

    if ( ModoPreparo.length > 0 ) {

        const ModoPreparoJSX = ModoPreparo.map ( ( item ) => {

            return ( <p key={ ind++ }> { item }<br/> </p> ); 

        } );

        return ModoPreparoJSX;
    } 
    else
    {
        return null;
    }        
};

export default MostraModoPreparo;