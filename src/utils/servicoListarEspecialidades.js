import axios from '../axios/axios';

const servicoListarEspecialidades = async (configurarState) => {

    try 
    {
        const resposta = await axios.get("especialidade")

        console.log( `Resposta : ${ JSON.stringify( resposta.data ) } ` )

        const lista = resposta.data.especialidades.sort( ( a , b ) => {
            return (a.descricao > b.descricao) ? 1 : -1 } )

        configurarState( lista )

    } 
    catch (erro) 
    {
        if ( erro.message )
        {
            console.log(erro.message)
        }
        else
        {
            alert ("O servidor está fora do ar... Tente Mais Tarde...")
        }
    }
};

export default servicoListarEspecialidades;