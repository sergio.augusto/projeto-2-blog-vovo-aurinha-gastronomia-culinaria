import axios from '../axios/axios';

const servicoIncluirNovaReceita = async ( novaReceita ) => {

    try 
    {
        const resposta = await axios.post( "receita", novaReceita )

        console.log( `Resposta : ${ JSON.stringify( resposta.data ) } ` )

        return ( { 'sucesso' : true, 'mensagem' : "Nova Receita cadastrada com sucesso..." } )
    }
    catch ( error ) 
    {
        if ( error.response && error.response.status )
        {
            switch ( error.response.status ) 
            {
                case 409:
                    return ( { 'sucesso': false, 'mensagem' : error.response.data.message } )
                
                default:
                    return ( { 'sucesso': false, 'mensagem' : "Problemas no cadastramento da Nova Receita" } )
                    
            }
        }
        else
        {
            return ( { 'sucesso': false, 'mensagem' : "O servidor está fora do ar... Tente Mais Tarde..." } )
        }
    }
};

export default servicoIncluirNovaReceita;