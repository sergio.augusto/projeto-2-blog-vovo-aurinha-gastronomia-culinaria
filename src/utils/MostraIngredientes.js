import React from "react";

const MostraIngredientes = ( { Ingredientes } ) => {
            
    let ind = 1;

    if ( Ingredientes.length > 0 ) 
    {

        const IngredientesJSX = Ingredientes.map ( ( item ) => {

            return ( <p key={ ind++ }> { item }<br/> </p> ); 

        } );

        return IngredientesJSX;
    } 
    else
    {
        return null;
    }        
};

export default MostraIngredientes;
