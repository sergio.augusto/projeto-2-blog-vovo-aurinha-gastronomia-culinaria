const COR_PADRAO = "Branco";
const COR_1      = "Rosa";
const COR_2      = "Verde";
const COR_3      = "Azul";

const COR_PADRAO_CONFIG = 
{
    corFundoTema : "white",
    corFundoBtnContinueLendo : "rgb(139,0,0)",
    corTextoBtnContinueLendo : "white",
    corTexto : "rgb(139,0,0)"
};

const COR_1_CONFIG = 
{ 
    corFundoTema : "rgb(255,192,203,75%)",
    corFundoBtnContinueLendo : "rgb(220,20,60)",
    corTextoBtnContinueLendo : "white",
    corTexto : "rgb(220,20,60)" 
};

const COR_2_CONFIG = 
{ 
    corFundoTema : "rgb(152,251,152,45%)",
    corFundoBtnContinueLendo : "rgb(0, 100, 0)",
    corTextoBtnContinueLendo : "white",
    corTexto : "rgb(0, 100, 0)" 
};

const COR_3_CONFIG = 
{ 
    corFundoTema : "rgb(135,206,250,70%)",
    corFundoBtnContinueLendo : "rgb(0,0,139)",
    corTextoBtnContinueLendo : "white",
    corTexto : "rgb(0,0,139)" 
};

export 
{
    COR_PADRAO,
    COR_1,
    COR_2,
    COR_3
};

export 
{
    COR_PADRAO_CONFIG,
    COR_1_CONFIG,
    COR_2_CONFIG,
    COR_3_CONFIG,
};
