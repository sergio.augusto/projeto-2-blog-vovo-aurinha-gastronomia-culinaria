import axios from '../axios/axios';

const servicoListarReceitas = async (configurarState) => {

    try 
    {
        const resposta = await axios.get("receita")

        console.log( `Resposta : ${ JSON.stringify( resposta.data ) } ` )

        configurarState( resposta.data.receitas )
    }
    catch (erro) 
    {
        if ( erro.message )
        {
            console.log(erro.message)
        }
        else
        {
            alert ("O servidor está fora do ar... Tente Mais Tarde...")
        }      
    }
};

export default servicoListarReceitas;