import axios from '../axios/axios';

const servicoListarReceitasPorEspecialidade = async ( idEspecialidade ) => {

    try 
    {
        const resposta = await axios.get(`especialidade/${ idEspecialidade }/receita`)

        console.log( `Resposta servico listar Receitas Por Especialidades : ${ JSON.stringify( resposta.data ) } ` )

        return  { 'sucesso': true, 'data': resposta.data.consulta, 'mensagem': "" } 
    }
    catch (error) 
    {
        if ( error.response && error.response.status )
        {
            switch ( error.response.status ) 
            {
                case 404:
                    return { 'sucesso': false, 'data':[], 'mensagem': error.response.data.message };


                default:
                    return { 'sucesso': false, 'data':[], 'mensagem': "Ocorreu algum problema em Listar Receitas por Especialidade." };

            }
        }
        else
        {
            return { 'sucesso': false, 'data':[], 'mensagem': "O servidor está fora do ar... Tente Mais Tarde..." };
        }  
    }
};

export default servicoListarReceitasPorEspecialidade;