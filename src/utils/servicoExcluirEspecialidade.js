import axios from '../axios/axios';

const servicoExcluirEspecialidade = async ( IdEspecialidade ) => {

    try 
    {
        const resposta = await axios.delete(`especialidade/${ IdEspecialidade }`)

        console.log( `Resposta : ${ JSON.stringify( resposta.data ) } ` )

        return ( { 'sucesso': true, 'mensagem': "Especialidade Excluída." } )
    }
    catch ( error ) 
    {
        if ( error.response && error.response.status )
        {
            switch ( error.response.status ) 
            {
                case 409:
                    return ( { 'sucesso': false, 'mensagem': error.response.data.message } );

                default:
                    return ( { 'sucesso': false, 'mensagem': "Ocorreu algum problema na exclusão da Especialidade." } );   
            }
        }
        else
        {
            return ( { 'sucesso': false, 'mensagem': "O servidor está fora do ar... Tente Mais Tarde..." } ); 
        }
    }
};

export default servicoExcluirEspecialidade;