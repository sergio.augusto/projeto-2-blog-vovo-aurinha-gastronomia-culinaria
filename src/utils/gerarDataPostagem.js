const gerarDataPostagem = () => {

    const hoje = new Date();
    const dia = hoje.getDate();
    const mes = hoje.getMonth() + 1 ;
    const ano = hoje.getFullYear();
    const diaFormatado = ( dia < 10 ) ? "0" + String (dia) : String (dia);
    const mesFormatado = ( mes < 10 ) ? "0" + String (mes) : String (mes);
    return ( diaFormatado + "/" + mesFormatado + "/" + ano );

};

export default gerarDataPostagem;