import axios from '../axios/axios';

const servicoAlterarReceita = async ( receitaId, receitaAlterada ) => {

    try 
    {
        const resposta = await axios.put( `receita/${receitaId}`, receitaAlterada )

        console.log( `Resposta : ${ JSON.stringify( resposta.data ) } ` )

        alert ( "Obrigado. Avaliação Registrada Com Sucesso..." )
    }
    catch ( error ) 
    {
        if ( error.response && error.response.status )
        {
            switch ( error.response.status ) 
            {
                case 409:
                    alert ( error.response.data.message )
                break
                
                default:
                    alert ( "Problemas na alteração da Receita" )
                break

            }
        }
        else
        {
            alert ( "O servidor está fora do ar... Tente Mais Tarde..." )
        }
    }
};

export default servicoAlterarReceita;